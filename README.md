
# ✈️ Capital2

Repositório destinado ao segundo exercício-programa da disciplina **PMR3304 — Sistemas de Informação** da Poli-USP. Trata-se de uma extensão do [primeiro exercício-prorama](https://gitlab.com/gvmossato/capital), agora utilizando [Django](https://www.djangoproject.com/) para implementação de um *backend*.

# 📝 Tarefas

Alguns dos requisitos e restrições da implementação, segmentada em três grandes grupos.

## Parte 1: Blog

✔️ Não usar extensões Django;

✔️ Usar um template base, que deve estar na pasta `/templates`;

✔️ Apresentar no mínimo um modelo com o nome de `Post`, que deve ter ao menos título, conteúdo e data de postagem, onde:
* O conteúdo da postagem deve ser armazenado como código HTML, que pode ser renderizado no template com o filtro `safe`;  
* A data de postagem deve ser um campo tipo `DateTimeField`, que pode ser renderizado no template com o filtro `date`;  
* O modelo deve estar registrado no administrador do Django.

✔️ Criar o repositório Git, ignorando o banco de dados `db.sqlite3` e a pasta `__pycache__/`;

✔️ Para as *views*, que devem implementar as operações CRUD, desenvolver três versões, cada uma em um *commit* diferente:
* Apenas com *views* funcionais, sem utilizar Django forms e sem validação de dados;
* Apenas com *views* funcionais, utilizando um Django form;
* *Views* com as classes genéricas:
  * `ListView`;
  * `DetailView` (responder com `404` caso seja requisitado um post inexistente);
  * `CreateView`;
  * `UpdateView`;
  * `DeleteView` (implementar uma página de confirmação).

✔️ Os links do template devem utilizar o nome das URLs configuradas no arquivo `urls.py`;

✔️ Deve existir ao menos um link nos templates para cada view.

## Parte 2: Comentários

✔️ Crie um *branch* chamado comments para o desenvolvimento das funcionalidades desta parte que, ao final desta etapa, deve ser feito o *merge* com o *branch* principal;

✔️ Apresentar um modelo com o nome de `Comment`, que deve conter o autor, texto e data de postagem, onde:
* O autor referencia o modelo [User](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#referencing-the-user-model) do Django;
* A data de postagem deve ser um campo tipo `DateTimeField`, que pode ser renderizado no template com o filtro `date`;
* O modelo deve estar registrado no administrador do Django.

✔️ Deve exibir os comentários no template individual de *posts*, ordenados pelo mais recente primeiro;

✔️ Conter uma *view* de criação de comentários, possuindo um link para ela na página individual do *post*;

✔️ Os links do template devem utilizar o nome das URLs configuradas no arquivo `urls.py`;

🔵 As *views* podem ser implementadas como função ou com classe genérica;

🔵 A criação e uso de classe *form* é opcional;

🔵 Não é necessário implementar a edição nem a remoção de comentários.

## Parte 3: Categorias

✔️ Crie um branch chamado `categories` a partir do principal (após o *merge* da seção anterior) para o desenvolvimento das funcionalidades desta parte; ao final, deve ser feito o *merge* com o *branch* principal;

✔️ Apresentar um modelo com o nome de `Category`, que deve conter o nome e uma descrição, e estar registrado no administrador do Django;

✔️ Implementar duas *views*: uma de listagem de categorias e outra individual de categoria, onde:
* Na *view* individual de categoria, responder com `404` caso seja requisitado um *post* inexistente;
* Na página individual de categoria, devem ser listados todos os *posts* pertencentes à categoria;
* Na página individual de *post*, exibir todas as categorias às quais o mesmo pertence;
* O template de listagem de *posts* da parte 1 deve ser reutilizado para renderizar a *view* individual de categoria, com adaptação para exibir o nome de categoria;

✔️ Para as *views*, criar os seguintes links de navegação:
* Link para a listagem de categorias no *header* do template base;
* Na página individual de *post*, cada categoria listada deve conter link para a página individual de categoria em questão;

✔️ Os links do template devem utilizar o nome das URLs configuradas no arquivo `urls.py`;

🔵 As *views* podem ser implementadas como função ou com classe genérica;

🔵 A criação e uso de classe *form* é opcional;

🔵 Não é necessário implementar a adição, edição nem a remoção de categorias na aplicação (use o modo administrador ou o *shell* para criar categorias).

# Resultado

*TO DO*
