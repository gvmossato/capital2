from django.conf import settings
from django.db import models


class Post(models.Model):
    name = models.CharField(max_length=255)
    info = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    picture = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name}'

    def get_absolute_url(self):
        return f'/places/{self.pk}' 


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    place = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date'] # Ordena pela coluna 'date', mais recente primeiro

    def __str__(self):
        return f'{self.author} at {self.date}'


class Category(models.Model):
    name = models.CharField(max_length=255)
    info = models.TextField()
    places = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}: {self.info}'
